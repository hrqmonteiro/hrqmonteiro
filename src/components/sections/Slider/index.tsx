/* eslint-disable @next/next/no-html-link-for-pages */
import {
  SiAmazonaws,
  SiCss3,
  SiExpress,
  SiHtml5,
  SiJavascript,
  SiMongodb,
  SiMysql,
  SiNextdotjs,
  SiNodedotjs,
  SiPostgresql,
  SiReact,
  SiRuby,
  SiRubyonrails,
  SiTailwindcss,
  SiTypescript,
} from 'react-icons/si'
import { BsBootstrapFill } from 'react-icons/bs'
import Menu from '../../ui/Menu'

export default function Slider() {
  return (
    <section
      id="slider"
      className="min-h-screen flex flex-wrap text-center justify-center py-4 items-center my-8"
    >
      <div className="container mx-auto px-6 lg:px-6">
        <div className="grid grid-cols-1 lg:grid-cols-2">
          <div>
            <div className="flex justify-center items-center text-left py-8 overflow-hidden">
              <div>
                <h1 className="text-2xl font-light mb-6">
                  O seu negócio
                  <br />
                  <span className="font-normal text-accent">
                    ainda não está na internet?
                  </span>
                </h1>
                <h2 className="text-6xl font-medium mb-10">
                  Você está
                  <br />
                  <div className="mt-3">
                    <div className="hidden lg:flex">
                      <span className="bg-accent bg-opacity-10 border border-accent p-2 font-bold text-accent">
                        perdendo clientes!
                      </span>
                    </div>
                    <div className="flex flex-wrap lg:hidden bg-accent bg-opacity-10 border border-accent p-2 font-bold text-accent">
                      <span className="w-full my-2">perdendo clientes!</span>
                    </div>
                  </div>
                </h2>
                <p className="font-mono mb-2">
                  Os primeiros segundos de uma marca são o que fixam na mente do
                  cliente!
                </p>
                <div className="flex justify-start">
                  <div className="grid grid-cols-1">
                    <div>
                      <div className="grid grid-cols-2 lg:flex lg:justify-start my-4">
                        <a href="/#contact">
                          <button className="lg:w-64 my-4 mr-4 btn btn-accent rounded-full lg:btn-lg lg:my-8 lg:mr-8">
                            <span className="text-xs lg:text-sm">
                              Solicitar Orçamento
                            </span>
                          </button>
                        </a>
                        <a href="/#portfolio">
                          <button className="lg:w-64 my-4 mr-4 btn rounded-full lg:btn-lg lg:my-8 lg:mr-8">
                            <span className="text-xs lg:text-sm">
                              Meus últimos projetos
                            </span>
                          </button>
                        </a>
                      </div>
                      <div className="flex items-center justify-start">
                        <div className="grid grid-cols-8 lg:grid-rows-2">
                          <div
                            data-tip="HTML5"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiHtml5 className="opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="CSS3"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiCss3 className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="JavaScript"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiJavascript className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="TypeScript"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiTypescript className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="Node.js"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiNodedotjs className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="Express"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiExpress className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="React"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiReact className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="Next.js"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiNextdotjs className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="Ruby"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiRuby className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="Ruby on Rails"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiRubyonrails className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="MongoDB"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiMongodb className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="MySQL"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiMysql className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="PostgreSQL"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiPostgresql className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="Bootstrap"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <BsBootstrapFill className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="Tailwind CSS"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiTailwindcss className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                          <div
                            data-tip="Amazon Web Services"
                            className="flex justify-center items-center tooltip tooltip-bottom mb-4 mr-4 md:mr-10 lg:mr-11"
                          >
                            <SiAmazonaws className="flex items-center opacity-40 hover:opacity-95 text-3xl my-4 mb-1 transition-all duration-300" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <img src="/images/macbook.jpg" alt="" />
          </div>
        </div>
      </div>
    </section>
  )
}

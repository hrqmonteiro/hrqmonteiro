import {
  RiFacebookLine,
  RiGithubLine,
  RiGitlabLine,
  RiInstagramLine,
  RiLinkedinLine,
  RiMastodonLine,
  RiRedditLine,
  RiYoutubeLine,
} from 'react-icons/ri'

export default function About() {
  return (
    <section
      id="about"
      className="w-full flex flex-nowrap min-h-screen justify-center items-center mb-24 lg:mb-40"
    >
      <div className="container mx-auto px-4">
        <div className="grid grid-cols-1 lg:grid-cols-2 gap-3">
          <div></div>
          <div>
            <div>
              <h2 className="text-5xl font-bold mb-2">Henrique Monteiro</h2>
              <span className="text-5xl font-thin">Full-Stack Developer</span>
              <p className="my-8">
                Desenvolvedor pleno, com foco em TypeScript, Node+Express,
                React+NextJS. Vasta experiência em criação de interfaces
                front-end, layouts focados em UI/UX, aplicativos com suporte PWA
                (Progressive Web Apps), criação e consumo de APIs REST e
                GraphQL.
                <br />
                <br />
                Uso extensivo de cloud, principalmente serviços da AWS (Amazon
                Web Services) como: S3, EC2, Lambda Functions, Amplify,
                DynamoDB, ElasticSearch, etc.
                <br />
                <br />
                Faço a sua aplicação completa (Front-end, Back-end, API, rotas,
                acesso ao banco de dados) com a melhor experiência para o
                usuário e pronta para rodar em qualquer dispositivo, trazendo
                assim 100% de conversões para o seu serviço ou produto!
              </p>
            </div>
            <div>
              <ul className="inline-flex">
                <li className="mr-4 scale-95 transform hover:scale-110 transition-all duration-50">
                  <a
                    className="text-xl"
                    href="https://github.com/hrqmonteiro"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <RiFacebookLine />
                  </a>
                </li>
                <li className="mr-4 scale-95 transform hover:scale-110 transition-all duration-50">
                  <a
                    className="text-xl"
                    href="https://github.com/hrqmonteiro"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <RiGithubLine />
                  </a>
                </li>
                <li className="mr-4 scale-95 transform hover:scale-110 transition-all duration-50">
                  <a
                    className="text-xl"
                    href="https://gitlab.com/hrqmonteiro"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <RiGitlabLine />
                  </a>
                </li>
                <li className="mr-4 scale-95 transform hover:scale-110 transition-all duration-50">
                  <a
                    className="text-xl"
                    href="https://instagram.com/hrqmonteiro"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <RiInstagramLine />
                  </a>
                </li>
                <li className="mr-4 scale-95 transform hover:scale-110 transition-all duration-50">
                  <a
                    className="text-xl"
                    href="https://linkedin.com/in/hrqmonteiro"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <RiLinkedinLine />
                  </a>
                </li>
                <li className="mr-4 scale-95 transform hover:scale-110 transition-all duration-50">
                  <a
                    className="text-xl"
                    href="https://fosstodon.org/@hrqmonteiro"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <RiMastodonLine />
                  </a>
                </li>
                <li className="mr-4 scale-95 transform hover:scale-110 transition-all duration-50">
                  <a
                    className="text-xl"
                    href="https://www.reddit.com/user/hrqmonteiro"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <RiRedditLine />
                  </a>
                </li>
                <li className="mr-4 scale-95 transform hover:scale-110 transition-all duration-100">
                  <a
                    className="text-xl"
                    href="https://www.youtube.com/channel/UCkTsjyvwCMO6ZsulBwNmUbQ"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <RiYoutubeLine />
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

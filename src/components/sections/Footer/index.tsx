import { SiGnu } from 'react-icons/si'
import { RiHeartFill } from 'react-icons/ri'

export default function Footer() {
  return (
    <section id="footer" className="bg-base-200 overflow-hidden">
      <div className="container mx-auto">
        <footer className="sm:text-sm py-4 px-2 lg:p-10 footer bg-base-200 text-base-content footer-center">
          <div className="grid grid-flow-col gap-4">
            <ul className="w-full flex flex-wrap">
              <li className="mx-4 uppercase font-medium border-b-4 border-base-200 hover:text-accent hover:border-accent py-2 px-2 transition-all duration-200">
                <a href="#slider">Home</a>
              </li>
              <li className="mx-4 uppercase font-medium border-b-4 border-base-200 hover:text-accent hover:border-accent py-2 px-2 transition-all duration-200">
                <a href="#about">Sobre Mim</a>
              </li>
              <li className="mx-4 uppercase font-medium border-b-4 border-base-200 hover:text-accent hover:border-accent py-2 px-2 transition-all duration-200">
                <a href="#portfolio">Portfolio</a>
              </li>
              <li className="mx-4 uppercase font-medium border-b-4 border-base-200 hover:text-accent hover:border-accent py-2 px-2 transition-all duration-200">
                <a href="#contact">Contato</a>
              </li>
            </ul>
          </div>
          <div>
            <div className="grid grid-flow-col gap-4">
              <div className="flex flex-wrap justify-center text-center">
                <div className="bg-black border-2 rounded-md text-white">
                  <div className="bg-white text-black border-base-300 h-6 text-xs p-2 w-full flex items-center justify-start">
                    Licensed under the
                  </div>
                  <div className="flex items-center justify-between px-4 py-2">
                    <SiGnu className="text-xl mr-2" />
                    <span className="font-bold text-xs my-2">GNU GPLv3</span>
                  </div>
                </div>
                <div className="w-full my-4">
                  <div className="w-full flex justify-center items-center">
                    Made with <RiHeartFill className="text-accent mx-1" /> using{' '}
                    <a
                      href="https://nextjs.org/"
                      target="_blank"
                      rel="noreferrer"
                      className="font-medium text-accent mx-1 hover:underline transition-all duration-200"
                    >
                      Next.js
                    </a>
                    &amp;{' '}
                    <a
                      href="https://tailwindcss.com/"
                      target="_blank"
                      rel="noreferrer"
                      className="font-medium text-accent mx-1 hover:underline transition-all duration-200"
                    >
                      TailwindCSS
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div className="grid grid-cols-1">
              <span className="text-5xl font-bold font-serif mb-4">HM</span>
              <span className="font-light">
                Henrique Monteiro - 2021
                <br />
                Todos os Direitos Reservados
              </span>
            </div>
          </div>
        </footer>
      </div>
    </section>
  )
}

export default function Contact() {
  return (
    <section
      id="contact"
      className="w-full flex flex-nowrap justify-center items-center my-20"
    >
      <div className="container mx-auto px-4">
        <div className="w-full">
          <h2 className="text-5xl font-bold mb-10 lg:mb-16">Contato</h2>
        </div>

        <div className="w-full">
          <div className="grid grid-cols-1 lg:grid-cols-2 gap-3">
            <form action="submit">
              <div className="w-full flex justify-start items-center">
                <input
                  type="text"
                  className="text-sm bg-transparent border-b-2 py-2 outline-none mr-10"
                  placeholder="Nome completo"
                />
                <input
                  type="email"
                  className="text-sm bg-transparent border-b-2 py-2 outline-none mr-10"
                  placeholder="Seu melhor e-mail"
                />
              </div>
              <div className="w-full flex justify-start items-center">
                <textarea
                  name="message"
                  id="message"
                  className="w-full text-sm bg-transparent border-b-2 py-2 outline-none mt-10 mr-10"
                  placeholder="Digite sua mensagem aqui..."
                  rows={14}
                ></textarea>
              </div>
              <div className="w-full flex justify-end">
                <button className="lg:w-64 my-4 mr-4 btn btn-accent rounded-full lg:btn-lg lg:my-8 lg:mr-8">
                  <span className="text-xs lg:text-sm">Enviar</span>
                </button>
              </div>
            </form>
            <div className="w-full flex flex-wrap justify-start items-start px-6">
              <span className="w-full text-xl font-light mb-2">
                Celular/WhatsApp:
              </span>
              <span className="w-full font-medium text-sm mb-10">
                <a
                  href="https://api.whatsapp.com/send/?phone=5517992811222&text&app_absent=0"
                  target="_blank"
                  rel="noreferrer"
                >
                  +55 17 99281-1222
                </a>
              </span>
              <span className="w-full text-xl font-light mb-2">E-mail</span>
              <span className="w-full font-medium text-sm mb-10">
                <a href="mailto:hrqmonteiro@protonmail.com">
                  hrqmonteiro@protonmail.com
                </a>
              </span>
              <span className="w-full text-xl font-light mb-2">Instagram</span>
              <span className="w-full font-medium text-sm mb-10">
                <a
                  href="https://instagram.com/hrqmonteiro"
                  target="_blank"
                  rel="noreferrer"
                >
                  @hrqmonteiro
                </a>
              </span>
              <span className="w-full text-xl font-light mb-2">LinkedIn</span>
              <span className="w-full font-medium text-sm mb-10">
                <a
                  href="https://linkedin.com/in/hrqmonteiro"
                  target="_blank"
                  rel="noreferrer"
                >
                  Henrique Monteiro
                </a>
              </span>
              <span className="w-full text-xl font-light mb-2">GitHub</span>
              <span className="w-full font-medium text-sm mb-10">
                <a
                  href="https://github.com/hrqmonteiro"
                  target="_blank"
                  rel="noreferrer"
                >
                  hrqmonteiro
                </a>
              </span>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

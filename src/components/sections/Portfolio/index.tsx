/* eslint-disable @next/next/no-img-element */
const WorkRight = (props: any) => {
  return (
    <div className="grid grid-cols-1 lg:grid-cols-2 gap-3 py-24">
      <div className="w-full flex flex-wrap justify-start items-center">
        <h3 className="text-3xl font-bold mb-2">{props.title}</h3>
        <p className="font-mono mb-2">{props.description}</p>
        <a href={props.url} target="_blank" rel="noreferrer">
          <button className="lg:w-64 my-4 mr-4 btn btn-accent rounded-full lg:btn-lg lg:my-8 lg:mr-8">
            <span className="text-xs lg:text-sm">Visitar</span>
          </button>
        </a>
      </div>
      <div>
        <img src={props.imageurl} alt={props.title} className="px-6" />
      </div>
    </div>
  )
}

const WorkLeft = (props: any) => {
  return (
    <div className="grid grid-cols-1 lg:grid-cols-2 gap-3 py-24">
      <div>
        <img src={props.imageurl} alt={props.title} className="px-6" />
      </div>
      <div className="w-full flex flex-wrap justify-start items-center">
        <h3 className="text-3xl font-bold mb-2">{props.title}</h3>
        <p className="font-mono mb-2">{props.description}</p>
        <a href={props.url} target="_blank" rel="noreferrer">
          <button className="lg:w-64 my-4 mr-4 btn btn-accent rounded-full lg:btn-lg lg:my-8 lg:mr-8">
            <span className="text-xs lg:text-sm">Visitar</span>
          </button>
        </a>
      </div>
    </div>
  )
}

export default function Portfolio() {
  return (
    <section
      id="portfolio"
      className="w-full flex flex-nowrap min-h-screen justify-center items-center mb-20"
    >
      <div className="container mx-auto px-4">
        <div className="w-full">
          <h2 className="text-5xl font-bold mb-10 lg:mb-16">
            Últimos trabalhos
          </h2>
        </div>

        <WorkRight
          title="Rockfeller Language Center"
          url="https://rockfeller.com.br"
          imageurl="/images/rockfeller.png"
          description={
            <p>
              Site para a escola de idiomas Rockfeller Language Center.
              <br />
              Tecnologias utilizadas: React (utilizando CRA), React Router,
              Bootstrap (Reactstrap), React-Icons, Node (+Nodemailer)
            </p>
          }
        />
        <WorkLeft
          title="Hidraumon Máquinas"
          url="https://hidraumon.com.br"
          imageurl="/images/hidraumon.png"
          description={
            <p>
              Landing Page + Site institucional para a empresa Hidraumon
              Máquinas, do ramo de máquinas hidráulicas.
              <br />
              <br />
              Tecnologias utilizadas:
              <br />
              <br />
              Back-end:
              <ul className="list-disc ml-6">
                <li>
                  Node + Express (Autenticação, Criação de usuários, API para
                  CRUD em um CMS: adição de produtos, edição de preços, ofertas,
                  mudanças no catálogo em geral), Nodemailer, Banco de dados
                  MySQL, TypeORM
                </li>
              </ul>
              <br />
              Front-end:
              <ul className="list-disc ml-6">
                <li>
                  NextJS (com SSR [Server-Side Rendering]), Bootstrap (React
                  Bootstrap), React-Icons
                </li>
              </ul>
            </p>
          }
        />
        <WorkRight
          title="Pixexid"
          url="https://pixexid.com"
          imageurl="/images/pixexid.png"
          description={
            <p>
              Webapp de sistema de imagens em stock gratuitas
              <br />
              <br />
              Tecnologias utilizadas:
              <br />
              <br />
              Back-end:
              <ul className="list-disc ml-6">
                <li>
                  Node + Express (Autenticação, Criação de usuários, API, Rotas,
                  conexão com banco de dados), Docker (deploy com
                  conteinerização, utilizando DockerHub), MongoDB
                </li>
              </ul>
              <br />
              Front-end:
              <ul className="list-disc ml-6">
                <li>
                  NextJS (com SSR [Server-Side Rendering]), TailwindCSS,
                  React-Icons
                </li>
              </ul>
            </p>
          }
        />
        <WorkLeft
          title="Engemarkc"
          url="https://engemarkc.com.br"
          imageurl="/images/engemarkc.png"
          description={
            <p>
              Landing Page + Site institucional para a empresa Engemarkc, do
              ramo de automação industrial.
              <br />
              <br />
              Tecnologias utilizadas:
              <br />
              <br />
              Back-end:
              <ul className="list-disc ml-6">
                <li>
                  Node + Express (Autenticação, Criação de usuários, API para
                  CRUD em um CMS: adição de produtos, edição de preços, ofertas,
                  mudanças no catálogo em geral), Nodemailer, MongoDB, Mongoose
                </li>
              </ul>
              <br />
              Front-end:
              <ul className="list-disc ml-6">
                <li>
                  NextJS (com SSR [Server-Side Rendering]), TailwindCSS,
                  React-Icons
                </li>
              </ul>
            </p>
          }
        />
        <WorkRight
          title="LAV - Laudo Audio Visual [1.0]"
          url="https://app.laudoaudiovisual.com.br"
          imageurl="/images/lav1.png"
          description={
            <p>
              Sistema para emissão e compartilhamento de laudos radiológicos
              inteligentes
              <br />
              <br />
              Tecnologias utilizadas:
              <br />
              <br />
              Portal feito em Angular, Node+Express, MongoDB [Sistema obsoleto,
              migramos para o portal 2.0]
            </p>
          }
        />
        <WorkLeft
          title="LAV - Laudo Audio Visual 2.0"
          url="https://develop.lav.app.br"
          imageurl="/images/lav2.png"
          description={
            <p>
              Sistema para emissão e compartilhamento de laudos radiológicos
              inteligentes
              <br />
              <br />
              Tecnologias utilizadas:
              <br />
              <br />
              Back-end:
              <ul className="list-disc ml-6">
                <li>
                  Node+Express (API, Autenticação, Criação de usuários),
                  Nodemailer, GraphQL, TypeScript
                </li>
                <li>
                  Deploy utilizando serviços da AWS (Amazon Web Services), como:
                  EC2, S3, Lambda Functions, DynamoDB, Amplify, CloudFormation,
                  Athena, ElasticSearch, Buckets, Quicksight
                </li>
                <li>CI/CD utilizando Amplify com base em GitOps</li>
              </ul>
              <br />
              Front-end:
              <ul className="list-disc ml-6">
                <li>
                  NextJS (com SSR [Server-Side Rendering]), TailwindCSS,
                  React-Icons, consumo da API em Express utilizando GraphQL
                </li>
              </ul>
            </p>
          }
        />
        <WorkRight
          title="help! Crédito Pessoal"
          url="https://help.com.br"
          imageurl="/images/help.png"
          description={
            <p>
              Landing Page para captação de leads + sistema de CRM para
              gerenciamento de leads, prospects e clientes
              <br />
              <br />
              Tecnologias utilizadas:
              <br />
              <br />
              Back-end:
              <ul className="list-disc ml-6">
                <li>
                  Node + Express (Autenticação, Criação de usuários, API, Rotas,
                  conexão com banco de dados), MongoDB, Mongoose, deploy na AWS
                  utilizando EC2 e S3 (+Buckets e Lambda Functions)
                </li>
              </ul>
              <br />
              Front-end:
              <ul className="list-disc ml-6">
                <li>
                  NextJS (com SSR [Server-Side Rendering]), TailwindCSS,
                  React-Icons
                </li>
              </ul>
            </p>
          }
        />
      </div>
    </section>
  )
}

import { useState } from 'react'

export default function Menu() {
  const [drawer, setDrawer] = useState(false)

  return (
    <>
      <div className="fixed top-0 w-full navbar mb-2 shadow-lg bg-neutral text-neutral-content">
        <div className="container mx-auto">
          <div className="flex-1 px-2 mx-2">
            <span className="text-4xl font-serif font-bold">HM</span>
          </div>
          <div className="flex lg:hidden">
            <button
              onClick={() => setDrawer(!drawer)}
              className="btn btn-square btn-ghost"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                className="inline-block w-6 h-6 stroke-current"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 6h16M4 12h16M4 18h16"
                ></path>
              </svg>
            </button>
          </div>
          <div className="hidden lg:flex">
            <ul className="inline-flex">
              <li className="mx-4 uppercase font-medium border-b-4 border-neutral hover:border-accent hover:text-accent py-2 px-2 transition-all duration-200">
                <a href="#slider">Home</a>
              </li>
              <li className="mx-4 uppercase font-medium border-b-4 border-neutral hover:border-accent hover:text-accent py-2 px-2 transition-all duration-200">
                <a href="#about">Sobre Mim</a>
              </li>
              <li className="mx-4 uppercase font-medium border-b-4 border-neutral hover:border-accent hover:text-accent py-2 px-2 transition-all duration-200">
                <a href="#portfolio">Portfolio</a>
              </li>
              <li className="mx-4 uppercase font-medium border-b-4 border-neutral hover:border-accent hover:text-accent py-2 px-2 transition-all duration-200">
                <a href="#contact">Contato</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  )
}

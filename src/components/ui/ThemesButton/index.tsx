import { useEffect, useState } from 'react'
import { themeChange } from 'theme-change'

export default function ThemesButton() {
  const [showMenu, setShowMenu] = useState(false)

  useEffect(() => {
    themeChange(false)
  }, [showMenu])

  return (
    <>
      <button
        onClick={() => setShowMenu(!showMenu)}
        className="fixed bottom-2 left-2 lg:bottom-5 lg:left-5 btn btn-circle"
      >
        <div className="flex justify-center items-center">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            className="inline-block w-6 h-6 stroke-current"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M7 21a4 4 0 01-4-4V5a2 2 0 012-2h4a2 2 0 012 2v12a4 4 0 01-4 4zm0 0h12a2 2 0 002-2v-4a2 2 0 00-2-2h-2.343M11 7.343l1.657-1.657a2 2 0 012.828 0l2.829 2.829a2 2 0 010 2.828l-8.486 8.485M7 17h.01"
            />
          </svg>
        </div>
      </button>
      {showMenu && (
        <div className="fixed bottom-16 lg:bottom-20 left-5">
          <ul className="menu text-left p-5 shadow-lg bg-base-200 rounded-box">
            <li className="menu-title">
              <span>Temas:</span>
            </li>
            <li>
              <button
                className="outline-none selected:outline-none focus:outline-none"
                data-set-theme="light"
              >
                Light
              </button>
              <button
                className="outline-none selected:outline-none focus:outline-none"
                data-set-theme="dark"
              >
                Dark
              </button>
            </li>
          </ul>
        </div>
      )}
    </>
  )
}

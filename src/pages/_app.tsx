import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Head from 'next/head'
import Menu from '../components/ui/Menu'
import Footer from '../components/sections/Footer'
import ThemesButton from '../components/ui/ThemesButton'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <Menu />
      <ThemesButton />
      <Component {...pageProps} />
      <Footer />
    </>
  )
}
export default MyApp

import type { NextPage } from 'next'
import Head from 'next/head'
import Slider from '../components/sections/Slider'
import About from '../components/sections/About'
import Portfolio from '../components/sections/Portfolio'
import Contact from '../components/sections/Contact'

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Henrique Monteiro</title>
      </Head>

      <Slider />
      <About />
      <Portfolio />
      <Contact />
    </>
  )
}

export default Home

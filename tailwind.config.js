module.exports = {
  purge: {
    content: [
      './src/pages/**/*.{js,ts,jsx,tsx}',
      './src/components/**/*.{js,ts,jsx,tsx}',
    ],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    fontFamily: {
      sans: ['Roboto'],
      mono: ['Fira Code'],
      serif: ['Playfair Display'],
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('daisyui')],
  daisyui: {
    themes: [
      'dark',
      'light',
      {
        nord: {
          primary: '#88C0D0',
          'primary-focus': '#88C0D0',
          'primary-content': '#2E3440',
          secondary: '#81A1C1' /* Secondary color */,
          'secondary-focus': '#81A1C1' /* Secondary color - focused */,
          'secondary-content':
            '#ECEFF4' /* Foreground content color to use on secondary color */,

          accent: '#F7DF1E' /* Accent color */,
          'accent-focus': '#88C0D0' /* Accent color - focused */,
          'accent-content':
            '#2E3440' /* Foreground content color to use on accent color */,

          neutral: '#3d4451' /* Neutral color */,
          'neutral-focus': '#2a2e37' /* Neutral color - focused */,
          'neutral-content':
            '#ffffff' /* Foreground content color to use on neutral color */,

          'base-100':
            '#15191D' /* Base color of page, used for blank backgrounds */,
          'base-200': '#15191D' /* Base color, a little darker */,
          'base-300': '#15191D' /* Base color, even more darker */,
          'base-content':
            '#D8DEE9' /* Foreground content color to use on base color */,

          info: '#2094f3' /* Info */,
          success: '#009485' /* Success */,
          warning: '#ff9900' /* Warning */,
          error: '#ff5724',
        },
      },
    ],
  },
}
